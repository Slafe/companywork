
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Проект WorkForCorp
 * Данный проект предназначен для расчета оптимальной работы компании: максимальное
 * извлечение прибыли за имеющееся время. Функционал проекта реализован в данном классе "Company".
 * Входные данные для обработки извлекаются из файла, имеющий адрес каталога "src/files/inputData.txt"
 * Результат работы программы записывается построчно в файл, имеющий адрес каталога "src/files/output.txt"
 *
 *
 *
 * @author Ischeykin A.A. 1520
 */
public class Company {

    public static void main(String[] args) {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream("src\\files\\inputData.txt")))) {
            String timeString;
            String workMassiveString;
            int time;
            ArrayList<Integer> work;
            int[] sortedWork;
            ArrayList<Integer> optimalWork;
            while ((timeString = reader.readLine()) != null) {
                workMassiveString = reader.readLine();
                time = Integer.parseInt(timeString);
                work = parseToIntList(workMassiveString);
                sortedWork = convertIntegers(work);
                Arrays.sort(sortedWork);
                optimalWork = profitWorkFinder(time, sortedWork);
                try (FileWriter writer = new FileWriter("src\\files\\output.txt", true)) {
                    writer.write((calculateSum(optimalWork)) + "\n");
                    System.out.println("Наибольшая прибыль среди работы " + work.toString() + " за " + time + " единиц времени - " + calculateSum(optimalWork));
                    writer.close();
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Метод calculateSum
     * Организует работу суммирования всех элементов коллекции {@code list}
     * @param list - коллекция типа {@code Integer}
     * @return sum - сумма элементов {@code list}
     */
    private static int calculateSum(ArrayList<Integer> list) {
        int sum = 0;
        for (int number : list) {
            sum += number;
        }
        return sum;
    }

    /**
     * Метод profitWorkFinder
     * Предназначен для нахождения наиболее выгодной работы среди общего списка работы{@code workList},
     * отталкиваясь от имеющегося времени {@code availableTime}
     * @param availableTime - доступное время {@code int}
     * @param workList - массив, представляющий общий список работы {@code int[]}
     * @return optimalWork - коллекция, представляющая оптимальную работу(наибольшая прибыль за определенное время)
     */
    private static ArrayList<Integer> profitWorkFinder(int availableTime, int[] workList) {
        ArrayList<Integer> optimalWork = new ArrayList<>();
        for (int timePoint = 1; timePoint <= availableTime; timePoint++) {
            for (int workIndex = workList.length - 1; workIndex >= workList.length - availableTime; workIndex--) {
                optimalWork.add(workList[workIndex]);
                timePoint++;
            }
        }
        return optimalWork;
    }


    /**
     * Метод "parseToIntList"
     * Предназначен для конвертирования строки line(подразумевается, что list представлен в виде последовательности чисел, разделенных запятой)
     * в коллекцию, содержащуюю объекты типа Integer
     * @param line - строка, представляющая последовательность чисел, разделенных запятой {@code String}
     * @return work - коллекция, содержащая объекты типа Integer {@code ArrayList<Integer>}
     */
    private static ArrayList<Integer> parseToIntList(String line) {
        ArrayList<Integer> work = new ArrayList<>();
        for (int index = 0; index < line.length(); index = index + 2) {
            try {
                work.add(Integer.parseInt(String.valueOf(line.charAt(index))));
            } catch (NumberFormatException e) {}
        }
        return work;
    }

    /**
     * Метод "convertIntegers"
     * Предназначен для конвертирования листа объектов типа Integer в массив типа int
     * @param list (Коллекция типа int)
     * @return massive (Массив типа int)
     */
    public static int[] convertIntegers(ArrayList<Integer> list) {
        int[] massive = new int[list.size()];
        for (int i=0; i < massive.length; i++)
        {
            massive[i] = list.get(i).intValue();
        }
        return massive;
    }
}

